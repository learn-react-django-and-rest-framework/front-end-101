import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import BaseList from './ReactListBlank';
import BaseForm from './ReactFormBlank';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div>
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="list">
              <Route index element={<BaseList />} />
            </Route>
            <Route path="form">
              <Route index element={<BaseForm />} />
            </Route>
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;