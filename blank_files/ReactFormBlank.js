import React, {useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';



function BaseForm () {
  const [dropdowns, setDropdowns] = useState([])

  const [name, setName] = useState("")
  const [dropdown, setDropdown] = useState("")

  const getData = async () => {
    const url = 'http://localhost:8000/dropdown/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setDropdowns(data.dropdowns);
    }
  }

  useEffect(()=> { 
    getData();
  }, [])

  let navigate = useNavigate()

  const handleSubmit = async (event) => {
    event.preventDefault();

    const routeChange = (path) =>{
      navigate(path)
    }

    const data = {};
    data.name = name;
    data.dropdown = dropdown;

    const url = `http://localhost:8000/name/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    
    if (response.ok) {
      setName("");
      setDropdown("");
      routeChange('/new_name')
    }
  }

  const handleNameChange = (e) => {
    setName(e.target.value);
  }

  const handleDropdownChange = (e) => {
    setDropdown(e.target.value);
  }

  
  return (
    <div>
      <h1>Create a new Thingy</h1>
      <form onSubmit={handleSubmit} id="create-human-form">
        <div>
          <input onChange={handleNameChange} value={name} placeholder="Name" required name="name" type="text" id="name" className="form-control" />
          <label htmlFor="name">Name</label>
        </div>
        <div>
          <select onChange={handleDropdownChange} value={dropdown} required name="dropdown" className="form-select" id="dropdown">
            <option value="">Choose Information</option>
            {dropdowns.map(dropdown => {
              return (
                <option key={dropdown.id} value={dropdown.id}>{dropdown.dropdown_name}</option>
              )
            })}
          </select>
        </div>
        <button>Submit</button>
      </form>
    </div>
  );
}



export default BaseForm;





 



