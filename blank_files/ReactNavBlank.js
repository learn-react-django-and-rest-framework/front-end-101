import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav>
        <NavLink to="/">Home</NavLink>
          <ul>
            <li>
              <NavLink to="/visits">Visits</NavLink>
            </li>
          </ul>
    </nav>
  )
}

export default Nav;
