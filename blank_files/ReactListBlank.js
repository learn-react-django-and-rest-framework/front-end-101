import {useState, useEffect } from 'react';

function BaseList(){

    const [items, setItems] = useState([]);


    async function fetchItems() {
        const response = await fetch('http://localhost:8000/project/items/');

        if (response.ok){
            const parsedJson = await response.json();
            setItems(parsedJson.items);
        }
    }

    useEffect(() => {
        fetchItems();
    }, []);


    return (
        <div>
            {items.map(item => {
                return (
                    <div>
                        <h3>{item.name}</h3>
                    </div>
                )
            })}
        </div>
    );
}
export default BaseList;