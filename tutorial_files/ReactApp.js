import { BrowserRouter, Routes, Route } from 'react-router-dom'; //Import BrowserRouter, Routes, and Route from React's Router DOM.
import MainPage from './ReactMainPage' //Import the default function MainPage from the file called ReactMainPage
import BaseList from './ReactList'; //Import the default function BaseList from the file called ReactList
import BaseForm from './ReactForm'; //Import the default function BaseForm from the file called ReactForm

function App() { //Declare a function called App
  return ( //Declare a return statement, meaning everything inside return("HERE!") will be displayed on the page.
    <BrowserRouter>
      <div>
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="list">
              <Route index element={<BaseList />} />
            </Route>
            <Route path="form">
              <Route index element={<BaseForm />} />
            </Route>
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App; //Export the App function as the default, meaning it will NOT need to be put in {curly brackets} when it is imported.





//Starting on line 8, the code is as follows:

// 8: Declare a tag called "BrowserRouter"; this turns the entire project into one massive app with connected pages. Normally in React, you would
//    have this App.js as the main page of your project, but in the case of Routers, you do not want that. This takes the App function, and it
//    activates all the other pages as paths.
// 9: Open a <div> tag, also known as a "divider."
// 10: Open a tag called <Routes>, which is what we imported from React-Router-DOM; this will give our web app all kinds of routes, or "links" it can go to.
// 11: Creates a "route path," labeled as "/". This means that when the user is just sitting on the homepage, this will be the first thing they see.
//      The "element" is set equal to a declaration of MainPage, which we have imported from ReactMainPage. The <Route> is closed at the end of the line, it
//      doesn't need a second tag.
// 12: Creates a SET of Route Paths that all start with "list". That means, if you wanted more than one item that was labeled "list," such as "list/new/" or
//      "list/all/", it would be inside that Route as "path="new"", etc.
// 13: Puts a singular route called BaseList in the tag. It is set to "index," meaning it is the home page of that path. Any other links starting with "list"
//      would go below it, labeled with that path.
// 14: Closes the Route Path of "list"
// 15: Opens a new set of routes called "form".
// 16: Puts a route in the "form" path called BaseForm, calling it from our imports.
// 17: Closes the Route Path of "form"
// 18: Closes all the Routes
// 19: Closes the <div> holding everything
// 20: Closes the <BrowserRouter>, completing the page.