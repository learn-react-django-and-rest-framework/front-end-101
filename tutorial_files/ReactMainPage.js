
function MainPage() { //Creates a function called MainPage
    return ( //Creates a "return" statement; everything inside will be displayed on the page.
      <div>
        <h1>Main Page Title</h1>
          <p>
            This page is under construction.
          </p>
      </div>
    );
  }
  
  export default MainPage; //Exports the MainPage function as the "default" function. This means it doesn't have to be inside {curly brackets} when you import it.


  //Starting at Line 4, this is everything inside the JSX (HTML lookalike!) code.


// 4: Opens a <div> tag. Everything on your page has to be inside ONE tag, so you can't add other things with the same level of indentation.
// 5: Opens an <h1> tag, then gives it the content "Main Page Title", then closes it with a </h1> tag. h1 stands for Header One, meaning it will be the largest text on screen.
// 6: Opens a <p> tag. This stands for "paragraph," and everything before the closing tag (</p>) will be "paragraph" text. Just normal sized font.
// 7: Gives the <p> tag something to do; this is the text that will show up on your screen.
// 8: Closes your paragraph tag with </p>
// 9: Closes your main <div> tag with </div>