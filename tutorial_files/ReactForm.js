import React, {useState, useEffect} from 'react'; //Imports your functions from the built-in functions of React. 
                                                  //{useState, useEffect} MUST be in curly brackets, as they are NOT the default.
import { useNavigate } from 'react-router-dom'; // Imports useNavigate from React's Router DOM





function BaseForm () { //Declare your New Function

  const [dropdowns, setDropdowns] = useState([]) //Part 1 of the Dropdown (casing)


  const [name, setName] = useState("")
  const [dropdown, setDropdown] = useState("") //part 2 of the Dropdown (info)



  const getData = async () => { // Async means it won't go until you tell it to
    const url = 'http://localhost:8000/dropdown/'; //where you get the Dropdown data from
    const response = await fetch(url); //Set the variable "response" to await the URL response

    if (response.ok) { //If the URL response is successful,
      const data = await response.json(); //Set the response to a JSON format, calling it "data"
      setDropdowns(data.dropdowns); //Set the Dropdown List information to the data called from the list
    }
  }



  useEffect(()=> { //Use this information on reload
    getData(); //Refers to the "getData" made above, telling the function to be run
  }, [])



  let navigate = useNavigate() //Sets the variable "navigate" to the function "useNavigate," letting that variable trigger the function



  const handleSubmit = async (event) => { //Creates a variable called handleSubmit, which will be used to tell the form what to submit

    event.preventDefault(); //Stops the page from submitting the automatic information

    const routeChange = (path) =>{ //creates a function to redirect to a new page when you submit the form
      navigate(path) //uses the variable "navigate" to trigger useNavigate, to the path given later as a parameter
    }

    const data = {}; //Creates a blank object called "data"
    data.name = name; //sets the title of the data to "name", then gives it the information you input for "name"
    data.dropdown = dropdown; //sets the title of the data to "dropdown", then gives it the information you input for "dropdown"

    const url = `http://localhost:8000/name/`; //where Data comes from

    const fetchConfig = { //Declaring a variable to grab the data from the Fetch and configure it into a JSON string (text you can use)
      method: "post", //Setting the method to "post", or in this instance, creating new data
      body: JSON.stringify(data), //Filling the information with "JSON String Data"
      headers: { //Creating a header for your information
        'Content-Type': 'application/json', // Said header information
      },
    };

    const response = await fetch(url, fetchConfig); // Setting the "response" to grab the URL and the information you Configured
    
    if (response.ok) { //Only continues if the "response" comes back with a 200 code, or simpler put, the server tells your browser that the information is "ok"
      setName(""); //uses the setName from the useState information, "re-setting" it to whatever it was before you entered info
      setDropdown(""); //uses the setDropdown from the useState information
      routeChange('/new_name') //sets the new path to the desired redirect
    }
  }



  const handleNameChange = (e) => { //If the function "handleNameChange" is called, 
    setName(e.target.value); //it will set the name in setName to whatever value is put in the field
  }



  const handleDropdownChange = (e) => { //Same as before, if the function "handleDropdownChange" is called,
    setDropdown(e.target.value); //it sets the Dropdown to whatever is put in the field
  }

  

  return ( //Returns the following information to the page, using JSX (which looks like HTML!)
    <div>
      <h1>Create a new Thingy</h1>
      <form onSubmit={handleSubmit} id="create-human-form">
        <div>
          <input onChange={handleNameChange} value={name} placeholder="Name" required name="name" type="text" id="name" className="form-control" />
          <label htmlFor="name">Name</label>
        </div>
        <div>
          <select onChange={handleDropdownChange} value={dropdown} required name="dropdown" id="dropdown">
            <option value="">Choose Information</option>
            {dropdowns.map(dropdown => {
              return (
                <option key={dropdown.id} value={dropdown.id}>{dropdown.name}</option>
              )
            })}
          </select>
        </div>
        <button>Submit</button>
      </form>
    </div>
  );
}



export default BaseForm; //Exports the "BaseForm," AKA the name of the function; this means you can use it in other files as an object!




// Starting at Line 66, the code means the following:

// 66: Open a "div" tag, or a divider. Everything in the return has to be inside ONE tag, so don't forget to make one big "div" to put your other tags in.
// 67: Opens an h1 tag, aka Header 1 (the biggest header there is), then closes it at the end. Everything inside is going to be Header Text on the screen.
// 68: Opens a form tag, which means everything inside will be a part of the form.
// 69: Opens a div tag to hold a single input field
// 70: Creates an input tag, which can open and close in the same < brackets />. the input tag is labeled "onChange={handleNameChange}", meaning when
//      the "input" from a user is changed, it uses our handleNameChange function from earlier. The value it changes is declared as "value={name}",
//      then a placeholder for the text bar is given. The other stuff just has to match the value, but the most important part is the "type=text" tag!!
//      Whatever "type" you have tells the JSX form what way the user can input info! You can have "text," "URL," "number," "datetime," and all sorts of
//      other cool stuff.
// 71: Creates a label related to the "name" input, puts a "label," (it will put "name" next to the text bar), then closes it.
// 72: Closes the div tag from line 69.
// 73: Opens a div tag for the next input field.
// 74: Rather than an input tag, it opens a "select" tag, which allows the user to choose their answer. Same as before, it has an "onChange" field, but this time,
//      we're making a dropdown, so it has the OTHER handleChange tag. The value is set to "dropdown," and the other difference is there is no "type." This is
//      because it's already declared as a "select" type in the beginning of the tag!
// 75: Creates an option called "Choose Information," which can't be selected, because the value is empty!
// 76: Starts a function. If you pay close attention, previously we've just used "dropdown" SINGULAR. Because we're collecting ALL the information from the
//      dropdown data and making a list of it, we need to use the OTHER useState, which we named "dropdowns" PLURAL. After calling "dropdowns," we .map() it,
//      which basically is a "for" loop. It says, "for every item in this list, do the following that many times," but kind of like the "HTML-ish" version.
//      Within the parenthesis of your "map()" function, you put the name of what you're mapping, which can be anything. You could map "banana" if you wanted,
//      as long as you were consistent. The map of "dropdown" is fat arrowed (told to do the following info) to...
// 77: ...a return statement, which means it can use JSX again. Inside of your return()...
// 78: ...there is another <option>. This time, it's set to the key and value of your "dropdown.id". This one is easy, because EVERYTHING in coding has an "ID."
//      Basically, it's telling Javascript to look at a dropdown, then check out its id. Next, OUTSIDE of the <option> tag, it has "{dropdown.name}". Essentially,
//      it's taking the dropdown, and checking for any value associated with it called "name." Then, the </option> tag closes the option.
//      To put it simply, the entire function is used to take every single name in the data collected previously, and make a list of them.
// 79: Closing the "return" statement.
// 80: Closing the rest of the "map" function's statements.
// 81: Closing the <select> tag from line 74.
// 82: Closes the <div> tag from line 73.
// 83: Creates a <button> tag, which, as you can probably guess, makes a button. Everything inside the two "button" tags is what text will be put on the button.
//      Automatically, the button is set to the function "submit;" coincidentally, you previously set your form to HANDLE submit functions! If you wanted your
//      form to do something else, you would use the "onClick={do this}" to change that.
// 84: Closes the <form> tag made on line 68.
// 85: Closes the last <div> tag.
// 86: Closes the "return" statement for your function
// 87: Closes the curly brackets for your entire function response.
