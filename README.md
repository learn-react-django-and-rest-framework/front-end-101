Welcome to Front End 101!

This is STRICTLY a tutorial for editing and familiarizing yourself with the file types.
After completing this, if you feel ready, follow the link at the bottom labeled "REAL REACT DJANGO PROJECT" and work from there.


In these documents, I have created several kinds of pages you will need to navigate through React JS, as well as a few additional pages that spice up a project.
There is NO back-end in this project! I suggest you use Django as your back-end framework, but I will soon be releasing another tutorial on how to use Django, if you DO need help in that department. Aside from that, there are two major segments to this project: one folder of Tutorial Files, which are all annotated, and a folder of Blank Files, which will help you get started if you learn best without that kind of notes. I suggest you start with the tutorial files.


THESE INSTRUCTIONS ARE ONLY FOR WINDOWS OS USERS! (I'm poor and don't have a Mac)



**TO SET UP**

Please ensure you have "Git" installed in your computer, from this link: https://gitforwindows.org/

In the GitLab project, search for an option in the Top Right called *Forks*. Click this.
Title the project whatever you'd like, then set your *Project URLs* namespace to **your username** from the dropdown. (This ensures it will be public!)
Click Fork Project at the bottom when you're ready.
Then, find the blue *Clone* button, and select it. Copy the link titled *Clone with HTTPS*, and keep it for the next step.

Open a *Terminal*. If you search your apps, you'll find one called "terminal"... There is a terminal that comes with your VSCode, but we don't suggest using that to start.

*Windows Terminal:*

    1. Open the terminal app. It looks intimidating, but it won't be after long.
    2. THIS STEP VARIES! TRY a. FIRST, THEN TRY b. IF YOU RECEIVE AN ERROR!
    
        a. type "cd desktop"
        b. type "cd onedrive" THEN "cd desktop"

Once you're in the right spot, type "git clone", then paste your URL you copied from the GitLab project, and click enter.
Congrats! You now have a version of this project that ONLY YOU can see on your device!

In your *Terminal*, type the following:

    cd <put the name of your project here WITHOUT spaces (try - or _)>

PIP will likely need to update, but feel free to ignore it - the rest of us do, too.
Once PIP has finished installing all your requirements, type "code ." - DON'T forget the period! It tells the computer to open SPECIFICALLY the folder you've selected in your code editor.

And there you go! You got the project ready to edit! Remember, these are good for reference, but won't actually create a project for you.



**TO USE**

In the folders attached, there's one called Tutorial Files, and one called Blank Files. Start with the Tutorial Files if you have questions about what needs to go into a React project. If you just saved this for the template, great! Pop into the Blank Files and copy away.


In Tutorial Files, you will find:

    1. Four files ending with ".js" - these are the bones of a React project, written in Javascript.
    2. Lots, and lots, and LOTS of notes. Hopefully these notes will help you figure out what to do in these many scenarios, but please remember that these do NOT work on their own. The projects will need a Back-End through Django, which I have a link for at the bottom.

In Blank Files, you will find:

    1. Four React.js files filled with the same information as the other folder.
    2. NO CSS file. Follow the link at the bottom for help with CSS for websites like you want!
    3. NO notes. This one is all you! They are also helpful resources for coding real projects in React.js!

Hop into the Tutorial Files and start poking around.

If you made something you really like and want to save it for later online...
Type the following lines, one by one, into your terminal:

    git add .
    git commit -m "Type here what you did with the project!"
    git push origin main

When it's finished loading, check out GitLab and see if it worked. Good job!


**WHEN YOU ARE DONE**

Congrats, you may (or may not) have a better understanding of React now! Our next step suggestions are:
    1. Check out our Back End 101 project, linked at the bottom, to learn what goes into a Django application.
    2. If you've already done that, go to the link titled "REAL REACT DJANGO PROJECT," and give your skills a shot.

Best of luck, baby coder!



LINK TO: Back-End-101
https://gitlab.com/learn-react-django-and-rest-framework/back-end-101

LINK TO: REAL REACT DJANGO PROJECT
https://gitlab.com/learn-react-django-and-rest-framework/real-react-django-project

LINK TO: Examples of CSS that are free to copy and use!
https://www.quackit.com/css/examples/